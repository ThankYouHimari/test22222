#!/bin/bash
#

# Chat ID
PD_KEY="e22c04de-7ce8-4e47-9294-d62e2e546307"
CHAT_ID="501464624"
FOX_DIR="$WORKSPACE_OUTPUT/fox_12.1"
OUT_DIR="$FOX_DIR/out/target/product/alioth"
BOT_TOKEN="1739544793:AAFaIlpNGaPy63DilT18UKdQ3Zs8IAnEo24"

push_msg() {
    curl -s -X POST "https://api.telegram.org/bot$BOT_TOKEN/sendMessage" -d "chat_id=$CHAT_ID&parse_mode=html&text=$1"
}

pdrain_uploader() {
    local FILE_PATH="$1"
    local FILE_NAME="${FILE_PATH##*/}"
    
    local response
    response=$(curl -# -F "name=$FILE_NAME" -F "file=@$FILE_PATH" -u :$PD_KEY "https://pixeldrain.com/api/file")

    local FILE_ID
    FILE_ID=$(echo "$response" | grep -Po '(?<="id":")[^"]*')

    push_msg "Download: https://pixeldrain.com/u/$FILE_ID"
}

builder() {
    export ALLOW_MISSING_DEPENDENCIES=true
    source build/envsetup.sh
    lunch twrp_alioth-eng && mka adbd bootimage -j$(nproc --all)
    pdrain_uploader "$OUT_DIR/OrangeFox-*.zip"
}

builder
